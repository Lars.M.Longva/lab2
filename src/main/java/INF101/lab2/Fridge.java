package INF101.lab2;

import java.util.ArrayList;
import java.util.List;

public class Fridge implements IFridge {
    int maxNumberOfItems = 20;

    ArrayList<FridgeItem> items = new ArrayList<>();


    @Override
    public int nItemsInFridge(){
        return items.size();
        }

    @Override
    public int totalSize() {
        return maxNumberOfItems;
        //hello jeg er
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() >= maxNumberOfItems) {
            return false;
        }
        items.add(item);
        return true;
        }

    @Override
    public void takeOut(FridgeItem item) {
        if (!items.contains(item)){
            throw new java.util.NoSuchElementException();
        }
        items.remove(item);
    }

    @Override
    public void emptyFridge() {
          items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();
        for (FridgeItem item : items){
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }
         items.removeAll(expiredItems);
        return expiredItems;
    }
}
